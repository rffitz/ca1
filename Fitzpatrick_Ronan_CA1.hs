--Ronan Fitzpatrick
--Function 1
--Takes in two numbers adds them and then doubles it
add_and_double::(Num a)=>a->a->a
add_and_double num1 num2 = (num1+num2)*2

--Function 2
--Uses an infix operator to add and double two numbers
--I figured this out after chatting with malachay
(+*) ::(Num a)=>a->a->a
(+*) num1 num2 = num1 `add_and_double` num2

--Function 3
--takes in a triple tuple which correspond to the numbers in
--a quadratic equation and then used them to find the root
-- I had trouble with this I tried it using the header
-- (Floating t) => t -> t ->t -> (t,t)
-- I kept getting an error saying there was no show function for this
-- so I googled and found an example here 
--http://rosettacode.org/wiki/Roots_of_a_quadratic_function#Haskell
-- I changed my header to take in a triple tuple based on this example
solve_quadratic_equation :: Floating t => (t,t,t) -> (t, t)
solve_quadratic_equation (a, b, c) = ((-b+x)/(2*a),(-b-x)/(2*a))
	where 
		x = (sqrt(b ** 2 - 4 * a * c))


--Function 4
--Takes a number and then returns a list of every number between it and one
first_n :: Int -> [Int]
first_n num = take num [1..]

--Function 5
-- Takes in an integer value and returns all integers between that integer and 1
first_n_integers :: Integer -> [Integer]
first_n_integers num 
	|num <= 0 = []
	|otherwise = take_integers num [1..]
	where
		take_integers num xs
			|num <= 0 = []
			|xs == []  = []
		take_integers num (x:xs) = x:take_integers (num-1) xs
	
--Function 6
--Takes in an integer and then works out the factorial and returns the factorial multiplied by every factorial under it
--eg: input = 3   output = 0!*1!*2!*3!

double_factorial:: Integer -> Integer
double_factorial 0 = 1
double_factorial n =  double_factorial (n-1)*factorial n
	where 
		factorial n
			|n == 0 = 1
			|otherwise = factorial (n-1) * n
		
--Function 7
--returns an infinite list of factorials
factorials :: [Integer]
factorials = 1 : zipWith  (*) factorials [1..]

--Function 8
--takes in an int and tests if the number is prime or not. It then returns a boolean true if it is
--and false if its not
isPrime:: Integer -> Bool
isPrime num = check num (num -1)
 	where
 		check num1 num2
 			|num2 <= 1 = True
 			|num1 `mod` num2 == 0 = False
 			|otherwise = check num (num2-1)
				
--Function 9
--returns an infinite list of prime numbers
primeList:: [Integer]
primeList = filter (isPrime) [1..]

--Function 10
--sums all the Ints in a list
sum_integers:: [Int] -> Int
sum_integers [] = 0
sum_integers (x:xs) = x + sum_integers xs


--Function 11
--This add all the numbers in a list and return the sum
--Found the solution in notes it did take a while to understand 
sum_integers_fold :: [Integer] -> Integer
sum_integers_fold = foldl (+) 0

--Function 12
--This finds the product of all the numbers in the list and returns the answer
-- I used what I learnt in the function above to write this one
product_integers_fold :: [Integer] -> Integer
product_integers_fold = foldr (*) 1

--Function 13
--takes in a string and a number and gives a responce based on your input
guess:: Int -> String -> String
guess num sentence
	|num > 5 = "You lose"
	|num < 5 && sentence == "I love functional programming" = "you win"
	|otherwise = "try again"

--Function 14
-- Takes in two lists of Nums it then multiplys each corresponding values in both arrays  and adds each pair to come up  with the dot product
--unsure 
dotProduct :: (Num a) => [a] -> [a] -> a
dotProduct [] _ = 0
dotProduct _ [] = 0
dotProduct (x:xs) (t:ts) = (x*t) + dotProduct xs ts

--Function 15
-- Takes a number and returns whether the number is even or not
-- Works for all numbers apart from neg
-- I googled the fromInteger Method
is_Even :: RealFrac a => a -> Bool
is_Even num = x == fromInteger (round x)
	where
		x = (num/2)
	
--Function 16
-- takes all vowels out of a string
unixname:: String -> String
unixname st =  [ c | c <- st, c `notElem` ['A','E','I','O','U','a','e','i','o','u']] 

--Function 17
--This takes in two lists and then finds what is common between the two lists
--now working got a bit of help off of Tom on this one
intersection::(Eq a) => [a] -> [a] -> [a]
intersection _ [] = []
intersection [] _ = []
intersection xs ts = [x|x <- xs,t <- ts, x ==t]

--Function 18
--takes in a string and replaces all of the vowels in it with an x
censor :: String -> String
censor word = map replace word
	where
		replace a
			|a `elem` ['A','a','E','e','I','i','O','o','U','u'] = 'x'
			|otherwise = a
	






